<?php

function leadlander_admin_settings($form, &$form_state) {
    $form['llactid'] = array(
        '#title' => t('llactid'),
        '#type' => 'textfield',
        '#default_value' => variable_get('llactid')
    );
    return system_settings_form($form);
}
